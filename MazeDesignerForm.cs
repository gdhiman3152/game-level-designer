/*Q-Puzzle Game
* Assignment 1
* Gurpreet Dhiman
* 7783152
* date of subbmission = 10/1/2018
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace GDhimanAssignment1
{
    public partial class MazeDesignerForm : Form
    {
        private const int INIT_TOP = 100;
        private const int INIT_LEFT = 250;
        private const int WIDTH = 60;
        private const int HEIGHT = 60;
        private const int VGAP = 0;
        private const int HGAP = 0;

        #region TOOL ITEMS
        //List of items in my tools
        enum MyList
        {
            Null,
            BlueBox,
            GreenBox,
            RedBox,
            YellowBox,
            BlueDoor,
            RedDoor,
            GreenDoor,
            YellowDoor,
            Wall
            
        }
        MyList list;
        #endregion
        public MazeDesignerForm()
        {
            InitializeComponent();
        }

        #region Generate Button
        /// <summary>
        ///  generate grid in Maze Designer form
        /// </summary>
        /// <param name="pbox"></param>
        private void btnGenerate_Click(object sender, EventArgs e)
        {
            try
            {
                int Rows = int.Parse(txtRows.Text);
                int Columns = int.Parse(txtColumns.Text);
                PictureBox[,] pbox = new PictureBox[Rows, Columns];
                Show(pbox);
            }
            catch (Exception ex)
            {

                MessageBox.Show(MessageBoxIcon.Exclamation + "::Please Provide Numbers" + "[" + ex.Message + "]" );
            }
         
        }

        private void Show(PictureBox[,] pbox)
        {
            int Rows = int.Parse(txtRows.Text);
            int Columns = int.Parse(txtColumns.Text);
            PictureBox[,] pb = new PictureBox[Rows, Columns];

            int x = INIT_LEFT;
            int y = INIT_TOP;
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    pb[i, j] = new PictureBox();
                    pb[i, j].Left = x;
                    pb[i, j].Top = y;
                    pb[i, j].Width = WIDTH;
                    pb[i, j].Height = HEIGHT;
                    pb[i, j].Visible = true;
                    pb[i, j].BorderStyle = BorderStyle.Fixed3D;
                    this.Controls.Add(pb[i, j]);
                    pb[i, j].Click += Grid_Click;
                    x += WIDTH + HGAP;
                   

                }
                x = INIT_LEFT;
                y += HEIGHT + VGAP;
               
            }
        }
        
        /// <summary>
        /// this use to drag images
        /// </summary>

        PictureBox modifyGrid = new PictureBox();
        private void Grid_Click(object sender, EventArgs e)
        {
            modifyGrid = (PictureBox)sender;

            switch(list)
            {
                case MyList.BlueBox:
                    modifyGrid.Image = GDhimanAssignment1.Properties.Resources.bbox;
                    break;
                case MyList.GreenBox:
                    modifyGrid.Image = GDhimanAssignment1.Properties.Resources.GBox;
                    break;
                case MyList.RedBox:
                    modifyGrid.Image = GDhimanAssignment1.Properties.Resources.RBox;
                    break;
                case MyList.YellowBox:
                    modifyGrid.Image = GDhimanAssignment1.Properties.Resources.YBox;
                    break;
                case MyList.BlueDoor:
                    modifyGrid.Image = GDhimanAssignment1.Properties.Resources.blue_door1;
                    break;
                case MyList.GreenDoor:
                    modifyGrid.Image = GDhimanAssignment1.Properties.Resources.doorG;
                    break;
                case MyList.RedDoor:
                    modifyGrid.Image = GDhimanAssignment1.Properties.Resources.doorR;
                    break;
                case MyList.YellowDoor:
                    modifyGrid.Image = GDhimanAssignment1.Properties.Resources.doorY;
                    break;
                case MyList.Wall:
                    modifyGrid.Image = GDhimanAssignment1.Properties.Resources.wallN;
                    break;
                case MyList.Null:
                    modifyGrid.Image = null;
                    break;

            }
        }


        #endregion


        #region Close Button 
        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion


        #region Save Button
        /// <summary>
        /// to save files
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {

            string str = "";
            str += this.txtRows.Text + this.txtColumns.Text + "\n";
            SaveFileDialog savefile = new SaveFileDialog();
            savefile.FileName = ".txt";
            savefile.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            //to save file in ".TXT" format
            if (savefile.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter sw = new StreamWriter(savefile.FileName))
                    sw.WriteLine(str);

                MessageBox.Show("File Saved");
            }
        }
        #endregion


        #region buttons
        /// <summary>
        /// to connect buttons with images
        /// and creating click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //select blue button from mylist
        private void btnBlueBox_Click(object sender, EventArgs e)
        {

            list = MyList.BlueBox;

        }
        //select green button from mylist
        private void btnGreenBox_Click(object sender, EventArgs e)
        {
            list = MyList.GreenBox;
        }
        //select red button from mylist
        private void btnRedBox_Click(object sender, EventArgs e)
        {
            list = MyList.RedBox;
        }
        //select yellow button from mylist
        private void btnYellowBox_Click(object sender, EventArgs e)
        {
            list = MyList.YellowBox;
        }
        //select blue door button from mylist
        private void btnBlueDoor_Click(object sender, EventArgs e)
        {
            list = MyList.BlueDoor;
        }
        //select green door button from mylist
        private void btnGreenDoor_Click(object sender, EventArgs e)
        {
            list = MyList.GreenDoor;
        }
        //select red door button from mylist
        private void btnRedDoor_Click(object sender, EventArgs e)
        {
            list = MyList.RedDoor;
        }
        //select yellow door button from mylist
        private void btnYellowDoor_Click(object sender, EventArgs e)
        {
            list = MyList.YellowDoor;
        }
        //select wall button from mylist
        private void btnWall_Click(object sender, EventArgs e)
        {
            list = MyList.Wall;
        }
        //select null button from mylist
        private void btnNone_Click(object sender, EventArgs e)
        {
            list = MyList.Null;
        }
        #endregion
    }
}
